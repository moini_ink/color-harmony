# Color Harmony Inkscape Extension

An extension for generating color palettes, starting from an object with a specific color, based upon the tool [Palette Editor](https://github.com/portnov/color-palette) by Ilya Portnov.

License: GPLv2 or any later
